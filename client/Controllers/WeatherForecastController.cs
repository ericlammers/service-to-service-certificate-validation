﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace client.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly HttpClient client;
        private readonly X509Certificate2 cert;

        public WeatherForecastController(HttpClient client, X509Certificate2 cert)
        {
            this.client = client;
            this.cert = cert;
        }

        [HttpGet]
        public async Task<IEnumerable<WeatherForecast>> Get()
        {
            var request = new HttpRequestMessage(HttpMethod.Get, "https://localhost:5001/weatherforecast");
            // request.Headers.Add("X-ARR-ClientCert", cert.GetRawCertDataString());
            
            var response = await client.SendAsync(request);
            
            return JsonSerializer.Deserialize<IEnumerable<WeatherForecast>>(await response.Content.ReadAsStringAsync());
        }
    }
}
